# ActivityPub basic tools

This library provides you with the fundamental building blocks to create an
activitypub server.

Included:
 - S2S request client for signing and sending AP events
 - S2S request validator for checking a request's signature and get the signing actor
 - Connect middleware for parsing and responding to webfinger requests
 - A basic function for stringifying json-ld objects.

As time goes by I plan to add more useful utility functions.

## Usage

```javascript
import http from 'http';
import { webfinger, stringify, Actor } from 'node-activitypub';
import generateRSAKeypair from 'generate-rsa-keypair';

const keys = generateRSAKeypair();

const domain = 'https://exampe.com';

const actor = new Actor({
	preferredUsername: 'actor',
	id: domain + '/@actor'
}, keys);

const client = actor.getClient();

const server = http.createServer(
	{},
	async (req, res) => {
		const url = new URL(req.url, `http://${req.headers.host}`);
		switch(url.href) {
			case actor.id:
				res.setHeader('content-type', 'application/json');
				res.end(stringify(actor));
				return;
			case actor.inbox:
				const signee = await client.verifySignature(req);
				let data = '';
				req.on('data', chunk => { data += chunk; })
				await new Promise(resolve => { req.on('end', resolve); });
				// Do something with signee & data
				res.end();
				return;
		}
		webfinger(username => username === actor.preferredUsername ? actor.id : null)(req, res);
	}
);

server.listen();

```

## Classes

- Actor: basic actor class that makes life a little easier
- Client: Make requests to Activitypub resources
- Object and collection classes: Helpers objects to make ActivityPub operations easier

More information in STRUCTURE.md

## Tests

Run tests with `npm test`.

## Examples

Examples to write:
- Interface for static sites
- Event generator
	- Input: ical url
	- Output:
		- Activitypub actor
		- Activitypub events on update
	- Create actor
	- Lookup actor (url or webfinger)
	- Accept follow requests
		- Validate incoming request
		- Insert into followers table (localActor, remoteActor)
		- Insert into outbox table (inbox, event)
	- Create events on cron
	- Send activities to followers
	- Lookup event details
	- Accept join/attending events
	- Process outbox queue
		- Send event to inbox
		- Remove from outbox table
- AP-Musicbrainz/Listenbrainz bridge
- Litepub
- Forum
- Bot
- 3D model hosting/sharing
- Follow -> Send T&C message -> Like/Vote -> Accept
