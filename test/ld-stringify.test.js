import stringify from '../src/ld-stringify.js';

const object = {
	actor: {
		'@context': 'actor-context',
		'id': 'http://example.com/'
	},
	object: {
		'@context': ['oc1', 'oc2'],
		'id': 'http://example.com/o',
		'tag': [
			{
				type: 'Note'
			}
		],
		"to": [
			"one",
			"two"
		]
	}
};

test('Stringify', () => {
	expect(stringify(object)).toMatchSnapshot();
});

test('Stringify with toJSON', () => {
	const o = {
		toJSON() {
			return object;
		}
	}
	expect(stringify(o)).toMatchSnapshot();
});
