import { send, stringify } from '../src/http';

test('Send helper', () => {
	const res = {
		setHeader: jest.fn(),
		send: jest.fn()
	}

	const o = {
		type: 'Person'
	}

	send(res, o);

	expect(res.setHeader).toHaveBeenCalledWith('content-type', 'application/activity+json');
	expect(res.send).toHaveBeenCalledWith(stringify(o));
});
