import webfinger from '../src/webfinger.js';

let lookup;
const middleware = webfinger((...args) => lookup(...args));

const req = {
	method: 'GET',
	url: '/.well-known/webfinger?resource=acct:paul@example.com',
	headers: {
		host: 'example.com'
	}
};

const res = {
	setHeader: jest.fn(),
	end: jest.fn()
};

test('Runs middleware', async () => {
	lookup = (u, h) => `http://${h}/@${u}`;
	await middleware(req, res);

	expect(res.end).toHaveBeenCalledWith('{"subject":"acct:paul@example.com","links":[{"rel":"self","type":"application/activity+json","href":"http://example.com/@paul"}]}');
});

test('Serves single actor', async () => {
	const actor = { preferredUsername: 'paul', id:"http://example.com/@paul" };
	const middleware = webfinger(actor);

	const req = {
		method: 'GET',
		url: '/.well-known/webfinger?resource=acct:paul@example.com',
		headers: {
			host: 'example.com'
		}
	};

	const res = {
		setHeader: jest.fn(),
		end: jest.fn()
	};
	const next = jest.fn();

	await middleware(req, res, next);

	expect(res.end).toHaveBeenCalledWith('{"subject":"acct:paul@example.com","links":[{"rel":"self","type":"application/activity+json","href":"http://example.com/@paul"}]}');
});


test('Calls next with errors', async () => {
	const e = new Error();

	const mw = webfinger(() => { throw e });

	const next = jest.fn();

	await mw(req, res, next);

	expect(next).toHaveBeenCalledWith(e);
});


test('Throws with no callback', async () => {
	const e = new Error();

	const mw = webfinger(() => { throw e });

	await expect(mw(req, res)).rejects.toThrow(e);
});
