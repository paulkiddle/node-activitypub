import createServer from '../../demo/index.js';

describe('basic server', () => {
	let inboxA, inboxB, a, b;

	beforeAll(async () => {
		inboxA = jest.fn();
		a = await createServer(inboxA);
		inboxB = jest.fn();
		b = await createServer(inboxB);
	});

	test('Webfinger lookup works', async () => {
		expect(await a.client.resolveWebfinger(b.webfinger, 'http')).toEqual(b.actor.id);
	});

	test('Actor lookup works', async () => {
		const expected = { ...b.actor };
		expected["@context"] = "https://www.w3.org/ns/activitystreams";
		expect(await a.client.get(b.actor.id)).toEqual(expected);
	});

	test('Sending to inbox works', async () => {
		const body = { test: 'test' };
		await a.client.post(b.actor.inbox, body);
		const expected = { ...a.actor };
		expected["@context"] = "https://www.w3.org/ns/activitystreams";
		expect(inboxB).toHaveBeenCalledWith(expected, JSON.stringify({ '@context':"https://www.w3.org/ns/activitystreams", ...body }, null, 2));
	});

	afterAll((youreMyWonderwall) => {
		a.server.close();
		b.server.close();
	});
});
