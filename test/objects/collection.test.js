import Collection from '../../src/objects/collection.js';

test('Collection', () => {
	const collection = Collection([1,2], { name: 'Test Collection' });
	expect(collection).toMatchSnapshot();
})

test('Collection defaults', () => {
	const collection = Collection();
	expect(collection).toMatchSnapshot();
})
