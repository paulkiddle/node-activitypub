import Actor from '../../src/objects/actor';

const keys = require('../keypair.json');
const actor = new Actor({ host: 'http://example.com', username: 'Paul' }, keys);


test('Actor', () => {
	expect(actor.getClient().constructor.name).toBe('Client');

	expect(actor.username).toBe(actor.preferredUsername);

	expect(actor.toJSON()).toMatchSnapshot();

	expect(actor.activity('Create', { type: 'Note', to:'Father Christmas' })).toMatchSnapshot();
});


test('Actor get and post', () => {
	const client = {
		get: jest.fn(),
		post: jest.fn()
	}

	const a = new Actor({}, keys, a=>client);

	a.get(1,2,3);
	expect(client.get).toHaveBeenCalledWith(1,2,3)

	a.post(4,5,6);
	expect(client.post).toHaveBeenCalledWith(4,5,6)
})

test('Actor accept', () => {
	const client = {
		post: jest.fn()
	}
	const activity = { actor: 'https://example.com/actor' };
	const inbox = 'https://example.com/inbox';

	const a = new Actor({}, keys, a=>client);

	a.accept(activity, inbox);
	expect(client.post).toHaveBeenCalledWith(inbox, {
		"@context": "https://www.w3.org/ns/activitystreams",
		type: 'Accept',
		to: activity.actor,
		object: activity,
		actor: a.id
	});
});

test('Actor accept no inbox', async () => {
	const targetActor = { inbox: 'https://example.com/inbox' };
	const client = {
		post: jest.fn(),
		get: jest.fn(() => targetActor)
	}
	const activity = { actor: 'https://example.com/actor' };

	const a = new Actor({}, keys, a=>client);

	await a.accept(activity);
	expect(client.get).toHaveBeenCalledWith(activity.actor);
	expect(client.post).toHaveBeenCalledWith(targetActor.inbox, {
		"@context": "https://www.w3.org/ns/activitystreams",
		type: 'Accept',
		to: activity.actor,
		object: activity,
		actor: a.id
	});
});
