import {protoForUrl, default as request} from '../../src/s2s/request';
import http from 'http';
import https from 'https';
import { throws } from 'assert';

test('Proto for url', () => {
	expect(protoForUrl('https://example')).toBe(https);
	expect(protoForUrl('http://example')).toBe(http);
	expect(protoForUrl(new URL('https://example.com'))).toBe(https);
	expect(protoForUrl({ toString() { return 'http://example.com'; }})).toBe(http);
	expect(() => {protoForUrl('mailto:paul@example.com')}).toThrow();
});

test('Request', () => {
	http.request = jest.fn();

	const args = ['http://example', 1];
	request(...args);

	expect(http.request).toHaveBeenCalledWith(...args);
});

let originalRequest;

beforeAll(() => {
	originalRequest = http.request;
});

afterAll(() => {
	http.request = originalRequest;
});
