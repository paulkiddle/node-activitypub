import { default as BaseClient, verifySignature, resolveWebfinger } from '../../src/s2s/base-client.js';

const mockRequest = {
	on: jest.fn(),
	setHeader: jest.fn(),
	getHeader: jest.fn(name => {
		return {
			'host': 'example.com',
			'date': 'Fri, 22 May 2020 12:54:19 GMT'
		}[name];
	}),
	end: jest.fn(),
	path: '/'
}

const mockResponse = (data = {}) => ({
	statusCode: 400,
	headers: {
		'content-type': 'application/activity+json'
	},
	on: (type, callback) => {
		if(type === 'data') {
			callback(JSON.stringify(data));
		} else if(type === 'end') {
			callback();
		}
	},
	setEncoding: jest.fn()
})

const mockHttp = responseData => ({
	request: jest.fn((url, options, callback) => {
		setTimeout(() => callback(mockResponse(responseData)), 0);
		return mockRequest;
	})
})

const client = new BaseClient(mockHttp());

test('Throws on invalid request body', () => {
	const e = new Error('Test error');
	return expect(client.httpRequest('', {}, () => {throw e})).rejects.toThrow(e);
})

test('Throws on 400 response', () => {
	return expect(client.httpRequest('', {})).rejects.toThrow();
})

test('Calls default fns', () => {
	BaseClient.verifySignature = jest.fn();
	BaseClient.resolveWebfinger = jest.fn();

	verifySignature();
	expect(BaseClient.verifySignature).toHaveBeenCalled();

	resolveWebfinger();
	expect(BaseClient.resolveWebfinger).toHaveBeenCalled();
});
