import Client from '../../src/s2s/client.js';

const keys = require('../keypair.json');
const privateKey = keys.private;

const mockRequest = {
	on: jest.fn(),
	setHeader: jest.fn(),
	getHeader: jest.fn(name => {
		return {
			'host': 'example.com',
			'date': 'Fri, 22 May 2020 12:54:19 GMT'
		}[name];
	}),
	end: jest.fn(),
	path: '/'
}

const mockResponse = (data = {}) => ({
	statusCode: 200,
	headers: {
		'content-type': 'application/activity+json'
	},
	on: (type, callback) => {
		if(type === 'data') {
			callback(JSON.stringify(data));
		} else if(type === 'end') {
			callback();
		}
	},
	setEncoding: jest.fn()
})

const mockHttp = responseData => ({
	request: jest.fn((url, options, callback) => {
		setTimeout(() => callback(mockResponse(responseData)), 0);
		return mockRequest;
	})
})

const client = new Client('http://key-id.example/', privateKey, mockHttp());

test('Signs requests', async () => {
	await client.get('http://example.com');

	expect(mockRequest.setHeader).toHaveBeenCalledWith('Signature', expect.any(String));
});



const publicKey = keys.public;

const mockIncomingRequest = {
	url: '/',
	headers: {
		signature: `keyId="http://key-id.example/",headers="(request-target) host date",signature="KHFc5dU3r0AtRcClhfVTropVRjcZME/0eb10BXM9mRYNKMsx6tLtdZ0uC9u1iLosIanm2df92C8bVQ7W8FuhvURVkdBho3p6Ieqtf0et84M1dVlUfZkUibRxSdI2WvXgGxuvNdyXmk+3bTnsCzYCu1FbCcJoIcpJL4JHvZJC9wanEo6QU/kzbYpPu/0SzZfLj5Fgz/ZO3X3rUtjxRR47yED3uFcyIQSwdxpiPGBFPSRB2O3T1yYZSXH4n+REK4zHnL5Yv1L8qtPgP6X55awxdMy4WF+n3s5TkGRP8+9+IhgtrTWN7wi7b+DDBWCSA+yr8j4v2qL1Xx1lGUbRKH5MEw=="`,
		host:'example.com',
		date: 'Fri, 22 May 2020 12:54:19 GMT'
	}
}

const mockActor = {
	publicKey: {
		publicKeyPem: publicKey
	}
};

test('Verify request', async () => {
	const client = new Client('http://key-id.example/', privateKey, mockHttp(mockActor));
	const actor = await client.verifySignature(mockIncomingRequest);

	expect(actor).toEqual(mockActor);
});

test('Reject bad signature', async () => {
	const mockIncomingRequest = {
		url: '/',
		headers: {
			signature: `keyId="http://key-id.example/",headers="(request-target) host date",signature="nope"`,
			host:'example.com',
			date: 'Fri, 22 May 2020 12:54:19 GMT'
		}
	}

	const client = new Client('http://key-id.example/', privateKey, mockHttp(mockActor));
	const actor = client.verifySignature(mockIncomingRequest);

	await expect(actor).rejects.toThrow('Actor signature could not be verified');
});

test('Reject no signature', async () => {
	const mockIncomingRequest = {
		url: '/',
		headers: {
			host:'example.com',
			date: 'Fri, 22 May 2020 12:54:19 GMT'
		}
	}

	const client = new Client('http://key-id.example/', privateKey, mockHttp(mockActor));
	const actor = client.verifySignature(mockIncomingRequest);

	await expect(actor).rejects.toThrow('Request not signed');
});


test('Reject no keyId', async () => {
	const mockIncomingRequest = {
		url: '/',
		headers: {
			signature: 'a=b',
			host:'example.com',
			date: 'Fri, 22 May 2020 12:54:19 GMT'
		}
	}

	const client = new Client('http://key-id.example/', privateKey, mockHttp(mockActor));
	const actor = client.verifySignature(mockIncomingRequest);

	await expect(actor).rejects.toThrow('Request signature has no keyId');
});

test('Get webfinger', async () => {
	const client = new Client('http://key-id.example/', privateKey, mockHttp({links:[{href:''}]}));
	jest.spyOn(client, 'resolveWebfinger');

	await client.get('paul@example.com');

	expect(client.resolveWebfinger).toHaveBeenCalledWith('paul@example.com');
});
