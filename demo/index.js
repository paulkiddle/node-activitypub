import http from 'http';
import { webfinger, stringify, Client } from '../dist/index.js';
import generateRSAKeypair from 'generate-rsa-keypair';

export default async (onInbox) => {
	const keys = generateRSAKeypair();
	const actor = {
		'@context': [],
		preferredUsername: 'actor',
		get inbox() {
			return this.id + '/inbox';
		},
		publicKey: {
			publicKeyPem: keys.public
		}
	};

	function getClient() {
		return new Client(actor.id, keys.private);
	}

	const server = http.createServer(
		{},
		async (req, res) => {
			const url = new URL(req.url, `http://${req.headers.host}`);
			switch(url.href) {
				case actor.id:
					res.setHeader('content-type', 'application/json');
					res.end(stringify(actor));
					return;
				case actor.inbox:
					const a = await getClient().verifySignature(req);
					let data = '';
					req.on('data', chunk => { data += chunk; })
					await new Promise(resolve => { req.on('end', resolve); });
					onInbox(a, data);
					res.end();
					return;
			}
			webfinger(username => username === actor.preferredUsername ? actor.id : null)(req, res);
		}
	);

	return new Promise((resolve, reject) => {
		try {
			server.listen(() => {
				try {
					const { port } = server.address();
					const host = `localhost:${port}`;
					const username = actor.preferredUsername;
					actor.id = `http://${host}/@${username}`;

					resolve({
						webfinger: `${username}@${host}`,
						client: getClient(),
						server,
						actor
					});
				} catch(e) {
					reject(e);
				}
			}).on('error', reject);
		} catch(e) {
			reject(e);
		}
	});
}
