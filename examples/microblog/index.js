/*
1. Create actor
2. Post as actor
3. Follow actors
4. Read actor inbox
*/
const {webfinger, stringify, Actor} = require('../../dist');
const express = require('express');

const port = process.argv[2] || 0;

const lcEqual = (a, b) => a.toLowerCase() === b.toLowerCase();

const collect = stream => new Promise((resolve) => {
	let str = '';
	stream.on('data', chunk => str += chunk);
	stream.on('end', () => resolve(str));
});

function normalise(object) {
	if(typeof object === 'string') {
		return {id: object};
	}

	for(const [key, value] of Object.entries(object)) {
		if(typeof value === 'object' && value.id && value.type) {
			object[key] = value.id;
		}
	}

	return object;
}

const app = express();
const actor = new Actor({
	preferredUsername: 'Paul'
});

const followers = [];
const inbox = [];

app.use(webfinger(user => lcEqual(user, actor.preferredUsername) ? actor : null));
app.get('/@:user', (req, res, next) => {
	if(lcEqual(req.params.user, actor.preferredUsername)) {
		// Can we do this in a better way?
		res.setHeader('content-type', 'application/activitypub+json');
		res.send(stringify(actor));
	} else {
		next();
	}
});

app.post('/@:user/inbox', async (req, res, next) => {
	try {
		if(lcEqual(req.params.user, actor.preferredUsername)) {
			const signee = normalise(await actor.getClient().verifySignature(req));

			const body = normalise(JSON.parse(await collect(req)));

			inbox.push(body);

			switch(body.type) {
				case 'Follow':
					if(body.actor === signee.id) {
						if(body.object === actor.id) {
							followers.push({
								id: signee.id,
								inbox: signee.inbox
							});
							actor.getClient().post(signee.inbox, {
								type: 'Accept',
								object: body.id,
								actor: actor.id
							});
						}
					}
			}

			res.sendStatus(204);
		} else {
			next();
		}
	} catch(e) {
		next(e);
	}
});

app.get('/', (req, res) => {
	res.send(`
	<html><body><form method=post action=/follow><input name=actor><button>Follow</button></form>
	<form method="post" action="/post"><textarea name=message></textarea><button>Post
	`);
});

app.post('/follow', async (req, res) => {
	const body = require('querystring').parse(await collect(req));

	const b = await actor.getClient().get(body.actor);
	const a = normalise(b);
	actor.getClient().post(a.inbox, {
		type: 'Follow',
		object: a.id,
		actor: actor.id
	});

	res.redirect('/');
});

app.post('/post', async (req, res) => {
	const body = require('querystring').parse(await collect(req));

	const aid = actor.id + "/" + Date.now();
	const post = {
		id: aid,
		type:	"Note",
		published:	(new Date()).toISOString(),
		url:	aid,
		attributedTo:	actor.id,
		to:	"https://www.w3.org/ns/activitystreams#Public",
		cc:	actor.id + "/followers",
		content:	body.message
	}

	const create = actor.activity('Create', post);

	for(const follower of followers) {
		actor.getClient().post(follower.inbox, create);
	}

	res.redirect('/');
});

app.get('/inbox', async (req, res) => {
	res.send('<pre>' + JSON.stringify(inbox, null, 2));
});

app.get('*', (req, res, next) => {
	console.log('Request for ' + req.path);
	next();
});

const server = app.listen(port, () => {
	const { port } = server.address();

	const host = `http://localhost:${port}`;

	console.log('Listening on', host);

	actor.host = host;
});

/*
const options = {
	appRoot: 'http://localhost:8082',
	autoAcceptFollows: true,
	async receiveAction(object, actor) {
		const handled = await super.receiveAction(object, actor);

		if(!handled) {
			inboxes[actor.username].push(object);
		}
	},
	resolver: new JsonResolver(__dirname + '/storage.json')
}

const storage = (() => { try { return require('./storage.json') } catch(e) { console.log(e) }  })();
const actors = storage ? Object.values(storage.objects).filter(a => a.username) : [];
const inboxes = Object.fromEntries(actors.map(a => [a.username, []]));

const ap = new ActivityPub(options);

const server = ap.createServer(
	{	secure: false	},
	(req, res, next) => {
		const url = new URL(req.url, `https://${req.headers.host}`);
		const actorList = `<select name="actor">${actors.map(a => `<option>${a.username}`).join('\n')}</select>`;
		switch(url.pathname) {
			case '/':
				res.end(`
<!doctype HTML>
<html>
<title>Microblog Example</title>
<body>
<form method="POST" action="/actor">
Create actor: <input name="actor"><button>Create</button>
</form>
<form action="/inbox">
Inbox for: ${actorList}
<button>Go</button>
</form>
<form method="POST" action="/follow">
Follow as: ${actorList}<br>
Follow: <input name="target">
<button>Follow</button>
</form>
<form method="POST" action="/post">
Post as: ${actorList}<br>
Message: <textarea name="message"></textarea><br>
<button>Ok</button>
</form>
				`);
				return;
			case '/actor':
				collect(req).then(async body => {
					const username = querystring.parse(body).actor;
					const actor = await ap.actor(username, true);
					actors.push(actor);
					inboxes[actor.username] = [];
					res.end(`<html>Created ${actor.id}. <a href="/">Continue</a>`);
				});
				return;
			case '/inbox':
				res.end(JSON.stringify(inboxes[url.searchParams.get('actor')]));
				return;
			case '/follow':
				collect(req).then(async body => {
					const { actor: username, target } = querystring.parse(body);
					const actor = await ap.actor(actors.find(a => a.username == username));
					actor.follow(target);
					res.statusCode = 303;
					res.setHeader('Location', '/');
					res.end();
				});
				return;
			case '/post':
				collect(req).then(async body => {
					const { actor: username, message } = querystring.parse(body);
					const actor = await ap.actor(actors.find(a => a.username == username));
					actor.broadcast(message);
					res.statusCode = 303;
					res.setHeader('Location', '/');
					res.end();
				});
				return;
		}
		next();
	}
);

server.listen(8082, () => console.log(`Listening on http://localhost:8082`));
*/
