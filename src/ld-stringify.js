/**
 * Quick and dirty LD stringify function. Needs revision.
 */
function stringify(object, spacing = 2) {
	const o = hoistContexts(object);
	if(o['@context'].length === 1) {
		o['@context'] = o['@context'][0];
	}

	return JSON.stringify(o, null, spacing);
}

function hoistContexts(object) {
	if(object.toJSON) {
		object = object.toJSON();
	}

	const clone = { };
	const ctx = object['@context'];
	const contexts = clone['@context'] = ctx ? (Array.isArray(ctx) ? ctx : [ctx]) : ["https://www.w3.org/ns/activitystreams"];

	for(const key in object) {
		if(key === '@context'){
			continue;
		}

		const value = clone[key] = object[key];
		if(Array.isArray(value)) {
			clone[key] = [];
			let i = 0;
			for(const v of value) {
				if(typeof v === 'object') {
					clone[key][i] = hoistContexts(v, contexts);
					contexts.push(...clone[key][i]['@context']);
					delete clone[key][i]['@context'];
				} else {
					clone[key][i] = v;
				}
				i++;
			}
		} else
		if(typeof value === 'object') {
			clone[key] = hoistContexts(value, contexts);
			contexts.push(...clone[key]['@context']);
			delete clone[key]['@context'];
		}
	};

	clone['@context'] = Array.from(new Set(contexts));

	return clone;
}

export default stringify;
