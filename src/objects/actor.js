import generateRSAKeypair from 'generate-rsa-keypair';
import Client from '../s2s/client'

/** ActivityStreams actor object */
class Actor {
	#keypair;
	#client;

	static paths = {
		id: u => `/@${u.preferredUsername}`,
		inbox: u => `${Actor.paths.id(u)}/inbox`,
		outbox: u => `${Actor.paths.id(u)}/outbox`,
		followers: u => `${Actor.paths.id(u)}/followers`,
		following: u => `${Actor.paths.id(u)}/following`,
	}

	/**
	 * Create a new instance of Actor
	 * @param {Object} options Actor attributes
	 * @param {Object} keypair Object containing public and private keys, as { public, private }
	 * @param {*} clientOpts Options to pass to the Client constructor
	 */
	constructor(options = {}, keypair = generateRSAKeypair(), clientOpts){
		this.#keypair = keypair;
		this.#client = clientOpts instanceof Function ? clientOpts(this) : new Client(
			{ toString: () => this.publicKey.id },
			keypair.private,
			clientOpts
		);

		const env = typeof process !== 'undefined' && process.env;

		Object.defineProperties(this, {
			paths: {
				configurable: true,
				writable: true,
				value: {}
			},
			host: {
				configurable: true,
				writable: true,
				value: env && env.host
			}
		})

		Object.assign(this, {
			type: 'Person'
		}, options);

		for(const [key, compile] of Object.entries(Actor.paths)) {
			Object.defineProperty(this.paths, key, {
				configurable: true,
				enumerable: true,
				get: () => compile(this).toLowerCase()
			});
			Object.defineProperty(this, key, {
				configurable: true,
				enumerable: true,
				get() {
					return this.host + this.paths[key]
				}
			});
		}
	}

	/**
	 * Alias of actor.preferredUsername
	 */
	get username() {
		return this.preferredUsername
	}

	/**
	 * Alias of actor.preferredUsername
	 */
	set username(value) {
		this.preferredUsername = value;
	}

	/**
	 * Get the instance of the actor's http client
	 */
	getClient(){
		return this.#client;
	}

	/**
	 * Get the JSON properties of the actor
	 */
	toJSON(){
		return Object.assign(
			{
				'@context': [
					"https://www.w3.org/ns/activitystreams",
					"https://w3id.org/security/v1"
				],
				inbox: this.inbox,
				publicKey: this.publicKey,
				followers: this.followers
			},
			this
		);
	}

	/**
	 * The public key object
	 */
	get publicKey(){
		return {
			id: this.id + '#main-key',
			owner: this.id,
			publicKeyPem: this.#keypair.public
		};
	}

	/**
	 * Create an ActivityStreams activity.
	 * `published`, `to` and `cc` fields are copied from the object to the activity.
	 * @param {string} type The `type` property of the activity
	 * @param {Object} object The `object` property.
	 */
	activity(type, object) {
		const activity = {
			type,
			actor: this.id,
			object
		};

		const copy = [
			'published',
			'to',
			'cc'
		];

		for(const key of copy) {
			if(object[key]) {
				activity[key] = object[key];
			}
		}

		return activity;
	}

	/**
	 * Create and send an Accept activity
	 * @param {Object} activity The activity to accept
	 * @param {String} inbox The inbox to send to
	 */
	async accept(activity, inbox = null) {
		if(!inbox) {
			inbox = (await this.#client.get(activity.actor)).inbox;
		}
		return this.#client.post(inbox, {
			"@context": "https://www.w3.org/ns/activitystreams",
			type: 'Accept',
			to: activity.actor,
			object: activity,
			actor: this.id
		});
	}

	/**
	 * Alias of Client.prototype.get
	 */
	get(...args){
		return this.#client.get(...args);
	}

	/**
	 * Alias of Client.prototype.post
	 */
	post(...args) {
		return this.#client.post(...args);
	}
/*
	send(to, body) {
		if (body) {
			body.to = to;
		} else {
			body = to;
		}

		return this._client.send(body);
	}

	/**
	 * Send an activity to the public collection and CC it to followers
	 * @param {Object} object Message to send
	 *
	async broadcast(object){
		if (object instanceof Promise) {
			object = await object;
		}

		if(typeof object === 'string') {
			object = {
				id: this._resolver.appRoot + '/' + crypto.randomBytes(16).toString('hex'),
				type: 'Create',
				published: (new Date).toISOString(),
				object: {
					id: this._resolver.appRoot + '/' + crypto.randomBytes(16).toString('hex'),
					type: 'Note',
					content: object,
					published: (new Date).toISOString(),
					attributedTo: this,
					cc: "https://www.w3.org/ns/activitystreams#Public"
				},
				actor: this
			}
		}
		object.to = Client.AS_PUBLIC;
		object.cc = this.followers;
		console.log(object);
		return this.send(object);
	}


	/**
	 * Send an activity to another recipient
	 * @param {string} to Optional recipient to send to
	 * @param {object} body Message to send
	 *
	async send(message) {
		const actor = this;
		// Recipients are in to, bto, cc, bcc
		const recipients = [].concat(
			message.to || [],
			message.cc || [],
			message.bto || [],
			message.bcc || []
		);

		// Sent message should not give away bto and bcc recipients
		delete message.bto;
		delete message.bcc;

		// Deduplicate destinations
		const sent = new Set();

		for(let recipient of recipients) {
			if(recipient === AS_PUBLIC) {
				// Public colleciton. Currently don't do anyhting special
				continue;
			}

			// If recipient is a string assume it's the recipient id; get the recipient then the inbox
			if(typeof recipient === 'string') {
				const id = recipient;
				recipient = await this.get(recipient);
				console.log('Fetched', recipient);

				if(!recipient) {
					console.warn(`Failed to get recipient ${id} from getObject call:`, recipient);
					continue;
				}
			}

			// Supposed to check that collections are owned by the recipient, should be enough to compare id
			if(String(recipient.id) === String(actor.followers.id)) {
				// Fetch followers from DB
				await actor.followers.load();
				console.log(...actor.followers.items)
				recipients.push(...actor.followers.items);
				continue;
			}

			// Dedupe
			if(sent.has(recipient.id)) {
				continue;
			}

			sent.add(recipient.id);

			if(!recipient.inbox) {
				console.error(`No inbox for recipient ${recipient.id}`);
				continue;
			}

			// Now actually send message
			const url = recipient.inbox.id || recipient.inbox;
			message['@context'] = actor.getContext();
			message.actor = actor;

			// Do we need to set TO?
			//message.to=recipient.id;

			await this.post(url, message);
		}
	}
	*/
}

export default Actor;
