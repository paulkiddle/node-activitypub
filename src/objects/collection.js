/**
 * Create an object representing a Collection
 * @param {Array} items The items in the collection
 * @param {Object} props Any additional properties for the collection
 */
export default function Collection(items = [], props = {}) {
	return {
		"@context": "https://www.w3.org/ns/activitystreams",
		type: "Collection",
		totalItems: items.length,
		items,
		...props
	}
}
