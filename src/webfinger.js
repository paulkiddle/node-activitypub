/**
 * An error resulting from parsing the webfinger request
 */
class WebfingerResourceError extends TypeError {
	get status() { return 400; }
}

function formatResult(result, subject) {
	if(!result) {
		return formatResult([]);
	}

	if(result.links) {
		return result;
	}

	if(Array.isArray(result)) {
		return { subject, links: result };
	}

	if(typeof result === 'string') {
		return {
			subject,
			links: [
				{
					rel: "self",
					type: "application/activity+json",
					href: result
				}
			]
		}
	}

	return {
		subject,
		links: [result]
	}
}

const defaultNext = e => {
	if(e) {
		throw e;
	}
}

/**
 * Lookup the user and return their ID or resource object
 * @callback FindResource
 * @async
 * @param {String} username The username of the resource being looked up
 * @param {String} host The host of the resource being looked up
 * @return {Object|String|Array} Either the entire webfinger response object, the array of links objects, or the user's href
 */

 /**
 * @typedef {Object} WebfingerOptions
 * @property {RexExp} matcher The regular expression to match the webfinger resource against. Must return 2 capture groups, for username and host respectively.
 * @property {hostTest}
 */

/**
 * Return middleware for handling a webfinger lookup
 * @param {FindResource} findResource The function that looks up and returns the user's ID
 * @param {Object} [WebfingerOptions] Advanced options
 * @returns {Function} Webfinger middleware
 */
export default function webfinger(findResource, options = {}){
	if(typeof findResource === 'object') {
		const actor = findResource;
		findResource = user => user === actor.preferredUsername ? actor.id : null
	}
	options = {
		protocol: 'http',
		matcher: /^acct\:([^@]+)@(.+)$/i,
		hostTest: (resourceHost, httpHost) => resourceHost === httpHost,
		...options
	}

	return async (req, res, next = defaultNext) =>	{
		const url = new URL(req.url, `${options.protocol}://${req.headers.host}`);

		if(url.pathname !== '/.well-known/webfinger' || req.method !== 'GET') {
			next();
			return false;
		}

		const resource = url.searchParams.get('resource');

		const match = resource && resource.match(options.matcher);

		if(!match) {
			next(new WebfingerResourceError('Webfinger expected a resource of the form "acct:username@domain.example"'));
		}

		const [, username, host] = match;

		try {
			const result = formatResult((!options.hostTest || await options.hostTest(host, url.host)) && await findResource(username, host), resource);

			res.setHeader('Access-Control-Allow-Origin', '*');
			res.setHeader('Content-Type', 'application/jrd+json');
			res.end(JSON.stringify(result));
			return true;
		} catch (e) {
			console.log(e);
			next(e);
		}
	}
}

export { WebfingerResourceError };
