import stringify from './ld-stringify';

export const AP_CONTENT_TYPE = 'application/activity+json'
export const AS_PUBLIC = 'https://www.w3.org/ns/activitystreams#Public';

export function send(res, object) {
	res.setHeader('content-type', AP_CONTENT_TYPE);
	res.send(stringify(object));
}

export { stringify };

export default { stringify, send };
