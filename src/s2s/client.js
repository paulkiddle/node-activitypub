import BaseClient from './base-client';
import stringify from '../ld-stringify.js';
import crypto from 'crypto';

export default class Client extends BaseClient {
	#publicKeyId
	#privateKey
	#console

	constructor(publicKeyId, privateKey, options = {}) {
		super(options);

		this.#publicKeyId = publicKeyId;
		this.#privateKey = privateKey;
		this.#console = options.console;
	}


	/**
	 * Send a signed HTTP(S) request with the activitypub accept type
	 * @param {String} url Destination
	 * @param {Object} options Http options
	 * @param {String} body Body to send
	 */
	async apRequest(url, options = {}, body = null) {
		return super.apRequest(url, options,
			req => {
				const meth = options.method || 'GET';
				this.#console.log(`Signed ${meth} ${url}: ${body}`);
				this.signRequest(req);
				req.end(body);
			}
		);
	}


	/**
	 * Send an activitypub POST request
	 * @param {String} url Destination
	 * @param {Object} body Body to send
	 */
	async post(url, body) {
		return this.apRequest(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/activity+json'
			}
		}, stringify(body))
	}

	/**
	 * Send an activitypub GET request
	 * @param {String} url Destination or webfinger
	 */
	async get(url) {
		if(url.match(BaseClient.webfingerRegex)) {
			url = await this.resolveWebfinger(url);
		}

		return this.apRequest(url);
	}

	/**
	 * Sign an outgoing http request
	 * @param {http.ClientRequest} req Outgoing request to sign
	 */
	signRequest(req){
		const pubKeyId = this.#publicKeyId;
		const privateKey = this.#privateKey;
		req.setHeader('Date', new Date().toUTCString());
		const headers = ['host', 'date'];
		const stringToSign = `(request-target): post ${req.path}\n` + headers.map(header => `${header}: ${req.getHeader(header)}`).join('\n');
		const signer = crypto.createSign('sha256');
		signer.update(stringToSign);
		const signature = signer.sign(privateKey).toString('base64');
		signer.end();

		req.setHeader('Signature', `keyId="${pubKeyId}",headers="(request-target) ${headers.join(' ')}",signature="${signature}"`);
		return req;
	}
}
