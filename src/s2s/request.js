export const protoForUrl = url => {
	/**
	 * Given a url, returns the correct node http library
	 * for making a request to that url
	 * @param {string} url Url
	 */
	if(url instanceof URL) {
		return require(url.protocol.slice(0, -1));
	}

	if(typeof url !== 'string') {
		url = String(url);
	}

	const proto = (p => p&&p[0])(url.match(/^https?(?=:)/));

	if(!proto) {
		throw new TypeError(`Expected http(s) protocol, got ${url}`);
	}

	return require(proto);
}

export default function request(url, ...args) {
	return protoForUrl(url).request(url, ...args);
}
