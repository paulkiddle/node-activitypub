import request from './request.js';

const trimQ = str => str.replace(/^"(.+)"$/g, '$1');

/**
 * Basic http client class for requests that don't
 * need an actor or a signature with a private key
 */
class Client {
	#console
	#request

	webfingerRegex = /^(?!https?:)@?(.+)@(.+)$/

	/**
	 * Create a new instance of the client.
	 * To use a custom http request function, specify options.request
	 * as a function that has the same signature has node's https.request
	 * To use a custom logging utility, specify options.console as an object
	 * that has the same signature as node's built-in `console` object.
	 * @param {Object} options Options object
	 */
	constructor(options = {}){
		this.#request = options.request || request;
		options.console = this.#console = options.console || console;
	}

	/**
	 * Send an HTTP(S) request
	 * @param {String} url Destination
	 * @param {Object} options Http options
	 * @param {String|Function} body Body to send or request modifying function
	 */
	async httpRequest(url, options = {}, body = null){
		const res = await new Promise((resolve, reject) => {
			const req = this.#request(url, options, resolve);
			req.on('error', reject);
			try {
				if(typeof body === 'function') {
					body(req);
					if(!req.writableEnded) {
						req.end();
					}
				} else {
					const meth = options.method || 'GET'
					this.#console.log(`${meth} ${url}: ${body}`);
					req.end(body);
				}
			} catch(e) {
				reject(e);
			}
		});


		let responseBody = '';
		res.setEncoding('utf8');
		res.on('data', chunk => responseBody += chunk);

		await new Promise(resolve => res.on('end', resolve));

		if(res.statusCode >= 400) {
			throw new Error(`Response status ${res.statusCode}: ${responseBody}`)
		}

		if(/\/(.+\+)?json(;|$)/.test(res.headers['content-type'])) {
			try {
				return JSON.parse(responseBody);
			} catch(e) {
				this.#console.warn(`Couldn't parse response body of type ${res.headers['content-type']}: ${responseBody}`);
			}
		}

		return responseBody;
	}

	/**
	 * Send an HTTP(S) request with the activitypub accept type
	 * @param {String} url Destination
	 * @param {Object} options Http options
	 * @param {String|Function} body Body to send or request modifying function
	 */
	async apRequest(url, options = {}, body = null) {
		return this.httpRequest(url, {
			...options,
			headers: {
				'Accept': 'application/activity+json',
				...options.headers
			}
		}, body);
	}

	/**
	 * Look up a webfinger and return the actor ID
	 * @param {String} target Webfinger handle to resolve
	 * @param {string} protocol Either http or https (default https)
	 */
	async resolveWebfinger(target, protocol = 'https') {
		const webfingerMatch = Client.webfingerRegex.exec(target);

		if(webfingerMatch){
			const domain = webfingerMatch[2];

			const webfinger = `${protocol}://${domain}/.well-known/webfinger?resource=acct:${target}`;
			const finger = await this.httpRequest(webfinger);

			const link = finger.links.find(l => l.type === 'application/activity+json') || finger.links[0];

			return String(new URL(link.href, protocol + '://' + domain));
		}
	}

	/**
	* Verify signature of incoming request is valid
	* @param {http.IncomingRequest} req Request to verify
	*/
	async verifySignature(url, req) {
		if(!req) {
			req = url;
			url = new URL(req.url, `https://${req.headers.host}`);
		} else {
			url = new URL(url);
		}

		// TODO: Require (request-target), host, date to be present
		// TODO: Make sure sign date is not too old.
		const signatureHeader = {};

		if(!req.headers.signature) {
			throw new Error('Request not signed');
		}

		// Extract keys and values from signature header
		for(const pair of req.headers.signature.split(',')) {
			const [, key, value] = pair.match(/^([^=]+)=(.+)/);
			signatureHeader[key] = trimQ(value);
		}

		// Signature tells us where to find public key and
		// which headers were used to sign the request
		const {keyId, headers} = signatureHeader;

		if(!keyId) {
			throw new Error(`Request signature has no keyId`);
		}

		const signature = Buffer.from(signatureHeader.signature, 'base64');

		// Get the public key
		const actor = await this.apRequest(keyId);

		const key = actor && actor.publicKey && actor.publicKey.publicKeyPem;

		// Build the string to match the signature against
		const comparisonString = headers.split(' ').map(name => {
			if(name === '(request-target)') {
				return `${name}: post ${url.pathname}`;
			} else {
				return `${name}: ${req.headers[name]}`;
			}
		}).join('\n');

		const verifier = require('crypto').createVerify('SHA256');
		verifier.write(comparisonString);
		verifier.end();

		if(verifier.verify(key, signature)) {
			return actor;
		}
		throw new Error('Actor signature could not be verified');
	}
}

Object.assign(Client, new Client);

export const verifySignature = (...args) => Client.verifySignature(...args);
export const resolveWebfinger = (...args) => Client.resolveWebfinger(...args);

export default Client;
