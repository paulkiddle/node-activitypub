Using node 12,

Run `npm init`

Run `npm install express git+https://gitlab.com/paulkiddle/node-activitypub.git#v0.3.3`

Edit index.js:

```javascript
const express = require('express');
const { Actor, webfinger, stringify } = require('activitypub');

const port = 3002;
const host = `http://localhost:${port}`;
const app = express();

const path = href => (new URL(href)).pathname;

const username = 'paul';
const me = new Actor({
	host,
	preferredUsername: username,
});

app.use(webfinger(user => user === username ? me.id : null));

app.get(path(me.id), (req, res) => {
	res.setHeader('content-type', 'application/activitypub+json');
	res.send(stringify(me));
});

app.listen(port, () => {
	console.log(host);
});
```
